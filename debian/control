Source: jcm
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <malat@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13), javahelper
Build-Depends-Indep: default-jdk-headless,
                     default-jdk-doc
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/jcm
Vcs-Git: https://salsa.debian.org/java-team/jcm.git
Homepage: https://www.nku.edu/~longa/classes/2010spring/mat129/days/applets/javamath/index.html

Package: libjcm-java
Architecture: all
Depends: ${misc:Depends},
         ${java:Depends}
Recommends: ${java:Recommends}
Suggests: libjcm-java-doc
Description: Java Components for Mathematics
 THE "Java Components For Mathematics" project represents an effort to develop a
 framework of configurable mathematical software components written in the Java
 programming language. These Java components are meant to be used on instructional
 Web pages as interactive illustrations, special-purpose calculators, support
 for exercises, and so forth. The components in Version 1 are mostly useful for
 calculus and pre-calculus and for science courses that use some of the same
 material. They use Java 1.1, and so will not work in some older browsers that
 support only Java 1.0.
 .
 This project was supported by NSF grant number DUE-9950473.

Package: libjcm-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${java:Depends}
Recommends: ${java:Recommends}
Suggests: libjcm-java
Description: Documentation for Java Components for Mathematics
 THE "Java Components For Mathematics" project represents an effort to develop a
 framework of configurable mathematical software components written in the Java
 programming language. These Java components are meant to be used on instructional
 Web pages as interactive illustrations, special-purpose calculators, support
 for exercises, and so forth. The components in Version 1 are mostly useful for
 calculus and pre-calculus and for science courses that use some of the same
 material. They use Java 1.1, and so will not work in some older browsers that
 support only Java 1.0.
 .
 This project was supported by NSF grant number DUE-9950473.
 .
 This package contains the API documentation of libjcm-java.
